import 'package:flutter/material.dart';

class Tarefa {
  Key id;
  String nome;
  DateTime data;
  bool concluida;

  Tarefa(this.nome, [this.concluida = false]) {
    id = new Key(DateTime.now().millisecondsSinceEpoch.toString());
    this.data = DateTime.now();
  }
}