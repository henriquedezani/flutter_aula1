import 'package:flutter/material.dart';
import 'tarefa.dart';
import 'package:intl/intl.dart';

void main() { 
  runApp(new HelloWorldApp());
}

class HelloWorldApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hello World App',
      home: new HomeScreen()
    );
  }
}



class HomeScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {

  List<Tarefa> tarefas = new List<Tarefa>();
  TextEditingController textCtrl = new TextEditingController();

  HomeScreenState() { 
    tarefas.add(new Tarefa("Lavar o carro"));
    tarefas.add(new Tarefa("Estudar para a prova"));
    tarefas.add(new Tarefa("Correr bastante"));
  }

  Widget getItem(Tarefa tarefa) {   

    IconData icon = (tarefa.concluida) ? Icons.check_box : Icons.check_box_outline_blank;
    Color iconColor = (tarefa.concluida) ? Colors.green : Colors.blue;

    return Dismissible(
        key: tarefa.id,
        onDismissed: (_) => deleteTarefa(tarefa),
        background: Container(
          color: Colors.red,
          child: Align(
            alignment: Alignment.centerRight,
            child: Icon(Icons.delete, color: Colors.white),
          )
        ),
        direction: DismissDirection.endToStart,
        child: GestureDetector(
      onTap: () => updateTarefa(tarefa),
      child: Container(
          color: Colors.white,
          padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          child: Row(
            children: <Widget>[
              Icon(icon, size: 42.0, color: iconColor),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,          
                  children: <Widget>[
                    Text(tarefa.nome),
                    Text(new DateFormat("d/M/y H:m").format(tarefa.data))
                  ],
                ),
              )
              
            ],
          ),
        ),
      ),
    );
  }

  void addTarefa(String texto) { 
    setState(() {
      tarefas.add(new Tarefa(texto));
      textCtrl.clear();
    });
  }

  void updateTarefa(Tarefa tarefa) { 
    setState(() {
      tarefa.concluida = !tarefa.concluida;
    });
  }

  void deleteTarefa(Tarefa tarefa) { 
    setState(() {
      tarefas.remove(tarefa);
    });
  }

  Widget createInput() { 
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blue),
        borderRadius: BorderRadius.all(Radius.circular(24.0)),
      ),
      padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.0),
      child: TextField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Digite sua tarefa"
        ),
        onSubmitted: addTarefa,
        controller: textCtrl,
      )
    );
  }

  @override 
  Widget build(BuildContext context) {
   return Scaffold(
        appBar: AppBar(
          title: Text("Hello World")
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: createInput()
            ),
            Expanded(
              child:ListView.builder(
                itemCount: tarefas.length,
                itemBuilder: (context, index) {
                  return getItem(tarefas[index]);
                },
              )
            )
            
          ],
        ),
      );
  }

}